<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyBillCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_bill_collections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_categories_id')->unsigned();
            $table->foreign('bill_categories_id')->references('id')->on('bill_categories');
            $table->integer('member_id')->unsigned();
            $table->foreign('member_id')->references('id')->on('members');
            $table->date('collection_date');
            $table->string('year');
            $table->string('month');
            $table->integer('payable_bill');
            $table->string('payment_options');
            $table->integer('bill_pay');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('monthly_bill_collections');
    }
}
