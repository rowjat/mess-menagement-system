<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table= 'members';
    public function allocate_rooms(){
        return $this->hasMany('App\Allocate_room');
    }
    public function monthly_bill_collections(){
        return $this->hasMany('App\Monthly_bill_collection');
    }
    public function monthly_advanced_collections(){
        return $this->hasMany('App\Monthly_advanced_collection');
    }
    public function new_members(){
        return $this->hasMany('App\dining\New_member');
    }
}
