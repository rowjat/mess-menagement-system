<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill_category extends Model
{
    protected $table= 'bill_categories';
    public function monthly_advanced_collections(){
        return $this->hasMany('App\Monthly_advanced_collection');
    }
    public function monthly_bill_collections(){
        return $this->hasMany('App\Monthly_bill_collection');
    }
}
