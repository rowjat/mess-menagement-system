<?php

namespace App\Http\Controllers\dining;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\dining\Cost;
use Session;
class CostController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $confirm = 0;
        $m =date('m',  strtotime(AjaxController::closing_date()));
        $y =date('Y',  strtotime(AjaxController::closing_date()));
        if($request->cost_type==true && $request->year==true && $request->month==true){
            $confirm = 1;
        }
        $m =$request->month;
        $y =$request->year;
        $start_request_month = date("$y-$m-01");
        $end_request_month = date("$y-$m-t");
        $datas = Cost::where('cost_type',$request->cost_type)->whereBetween('date', [$start_request_month, $end_request_month])->get();
        $report =$start_request_month.' To '.$end_request_month;
        
        return view('dining.cost.index')->withDatas($datas)->withConfirm($confirm)->withReport($report);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $m =date('m',  strtotime(AjaxController::closing_date()));
        $y =date('Y',  strtotime(AjaxController::closing_date()));
        $members =  \App\dining\New_member::where('month_of_join',$m)->where('year_of_join',$y)->get();
        return view('dining.cost.create')->withMembers($members);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
               $this->validate($request,[
            'date'=>'required|date|after:'.AjaxController::closing_date(),
            'vawter_no'=>'required',
            'cost_type'=>'required|integer',
              'drescription'=>'required' ,
                   'amount'=>'required|integer',
                   'spenders'=>'required|integer'
        ]);
      if($request->cost_type >0 && $request->spenders >0){
          $table= new Cost();
        $table->date = $request->date;
        $table->vawter_no = $request->vawter_no;
        $table->cost_type = $request->cost_type;
        $table->drescription = $request->drescription;
        $table->amount = $request->amount;
        $table->spenders = $request->spenders;
        $table->save();
        Session::flash('success','Data save  successfully !');
        return redirect()->route('dining_cost.create');
      }  else {
           Session::flash('fails','Pls Select field !');
        return redirect()->route('dining_cost.create');
      }
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
