<?php

namespace App\Http\Controllers\dining;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\dining\New_member;
use Session;
class MealController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $confirm = 0;
        $m =date('m',  strtotime(AjaxController::closing_date()));
        $y =date('Y',  strtotime(AjaxController::closing_date()));
        $members = New_member::where('month_of_join',$m)->where('year_of_join',$y)->get();
        if($request->member_id==true && $request->year==true && $request->month==true){
            $confirm = 1;
        }
        $m =$request->month;
        $y =$request->year;
        $start_request_month = date("$y-$m-01");
        $end_request_month = date("$y-$m-t");
        $datas = \App\dining\Meal::where('member_id',$request->member_id)->whereBetween('date', [$start_request_month, $end_request_month])->orderBy('date', 'asc')->get();
        $report =$start_request_month.' To '.$end_request_month;
        
        return view('dining.meal.index')->withDatas($datas)->withConfirm($confirm)->withReport($report)->withMembers($members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $m =AjaxController::closing_date();
        $m =date('m',  strtotime(AjaxController::closing_date()));
        $y =date('Y',  strtotime(AjaxController::closing_date()));
        $meal_members = \App\dining\New_member::where('month_of_join',$m)->where('year_of_join',$y)->get();
        
        
        return view('dining.meal.create')->withMeal_members($meal_members);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
//        $this->validate($request,[
//            'date'=>'required|date|after:'.AjaxController::closing_date()
//        ]);
        $x='';
        $y='';
        $input = $request->all();
        for($i = 0;$i<count($input['member_id']);$i++){
       $this->validate($request,[
            'date'=>'required|date|after:'.AjaxController::closing_date(),
            'no_of_meal'=>'required'  
        ]);     
      $table = new \App\dining\Meal();
      $table->date = $input['date'];
      $table->member_id = $input['member_id'][$i];
      $table->no_of_meal = $input['no_of_meal'][$i];
      $table->save();
        } 
        Session::flash('success','Data save  successfully !');
        return redirect()->route('dining_meals.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
