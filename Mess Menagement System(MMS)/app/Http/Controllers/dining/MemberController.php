<?php

namespace App\Http\Controllers\dining;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\dining\New_member;
use Session;
use App\Member;
class MemberController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
        if($request->month ==true && $request->year==true){
            $m = $request->month;
            $y = $request->year;
          $members = New_member::where('month_of_join',$request->month)->where('year_of_join',$request->year)->get();  
        }  else {
            $m =date('m',  strtotime(AjaxController::closing_date()));
            $y =date('Y',  strtotime(AjaxController::closing_date()));
            $members = New_member::where('month_of_join',$m)->where('year_of_join',$y)->get();
        }
        
        
       return view('dining.members.index')->withMembers($members)->withM($m)->withY($y);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $members = Member::where('present_condition','running')->get();
        return view('dining.members.create')->withMembers($members);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $this->validate($request,[
//            'month_of_join'=>'integer|between:1,12',
//            'year_of_join'=>'integer|between:1,31',
//            'member_id'=>'integer|min:1'
//        ]);
        if($request->member_id >0){
            $count = New_member::where('month_of_join',$request->month_of_join)->where('year_of_join',$request->year_of_join)->where('member_id',$request->member_id)->count();
            if($count >0){
                Session::flash('fails','This member Allready Taken !');
        return redirect()->route('dining_members.create');
            }  else {
        $table= new New_member();
        $table->month_of_join = $request->month_of_join;
        $table->year_of_join = $request->year_of_join;
        $table->member_id = $request->member_id;
        $table->save();
        Session::flash('success','Data save  successfully !');
        return redirect()->route('dining_members.create');
            }
            
        }else{
            Session::flash('fails','Pls Select field !');
        return redirect()->route('dining_members.create');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
