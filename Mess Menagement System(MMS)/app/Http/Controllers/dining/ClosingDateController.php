<?php

namespace App\Http\Controllers\dining;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\dining\Closing_date;
use Session;
class ClosingDateController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $closings = Closing_date::all();
        $count = $closings->count();
        return view('dining.closing_date.create')->withCount($count)->withClosings($closings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'date'=>'required|date'
        ]);
        
        $table= new Closing_date();
        $table->date = $request->date;
        $table->save();
        Session::flash('success','Data save  successfully !');
        return redirect()->route('dining_closing_date.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $closing_date = Closing_date::find($id);
        
        return view('dining.closing_date.edit')->withClosing_date($closing_date);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Closing_date::find($id);
        $table->date = $request->date;
        $table->save();
        Session::flash('success','Data save  successfully !');
        return redirect()->route('dining_closing_date.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
