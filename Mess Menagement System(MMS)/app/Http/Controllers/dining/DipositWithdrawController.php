<?php

namespace App\Http\Controllers\dining;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\dining\Diposit_or_withdraw;
use App\dining\New_member;
use Session;
use App\Http\Controllers\dining\AjaxController;
class DipositWithdrawController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $confirm = 0;
        $m =date('m',  strtotime(AjaxController::closing_date()));
        $y =date('Y',  strtotime(AjaxController::closing_date()));
        $members = New_member::where('month_of_join',$m)->where('year_of_join',$y)->get();
        if($request->member_id==true && $request->start_date==true){
            $confirm = 1;
        }
        
        $cashs = Diposit_or_withdraw::where('member_id',$request->member_id)->whereBetween('date', [$request->start_date, $request->end_date==true?$request->end_date:date("$y-$m-t")])->get();
        $report =$request->end_date==true?$request->start_date.' To '.$request->end_date:$request->start_date.' To '.date("$y-$m-t");
        
        return view('dining.diposit_withdraw.index')->withMembers($members)->withCashs($cashs)->withConfirm($confirm)->withReport($report);
    }
/*
 * 
 */
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $m =date('m',  strtotime(AjaxController::closing_date()));
        $y =date('Y',  strtotime(AjaxController::closing_date()));
        $members = New_member::where('month_of_join',$m)->where('year_of_join',$y)->get();
        return view('dining.diposit_withdraw.create')->withMembers($members);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
               $this->validate($request,[
            'member_id'=>'integer',
            'option'=>'integer|between:1,2',
            'date'=>'required|date|after:'.AjaxController::closing_date(),
              'amount'=>'required|integer'     
        ]);
      if($request->member_id >0){
          
          $table= new Diposit_or_withdraw();
        $table->member_id = $request->member_id;
        $table->option = $request->option;
        $table->date = $request->date;
        $table->amount = $request->amount;
        $table->save();
        Session::flash('success','Data save  successfully !');
        return redirect()->route('dining_diposit_withdraw.create');
      }  else {
           Session::flash('fails','Pls Select field !');
        return redirect()->route('dining_diposit_withdraw.create');
      }
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
