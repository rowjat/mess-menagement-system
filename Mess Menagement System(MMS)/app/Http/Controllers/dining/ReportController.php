<?php

namespace App\Http\Controllers\dining;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cost_rechived_payment(Request $request)
    {
        $confirm = 0;
        if($request->year==true && $request->month==true){
            $confirm = 1;
        }
        $m =$request->month;
        $y =$request->year;
        $start_request_month = date("$y-$m-01");
        $end_request_month = date("$y-$m-t");
        $report =$start_request_month.' To '.$end_request_month;
        
        $members = \App\dining\New_member::where('month_of_join',$m)->where('year_of_join',$y)->get();
        
        
       
        
        return view('dining.report.cost_rechived_payment')->withConfirm($confirm)->withReport($report)->withMembers($members)->withM($m)->withY($y);
    }

  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function member_wise_report(Request $request)
    {
      $confirm = 0;
        if($request->year==true && $request->month==true){
            $confirm = 1;
        }
        $m =$request->month;
        $y =$request->year;
        $start_request_month = date("$y-$m-01");
        $end_request_month = date("$y-$m-t");
        $report =$start_request_month.' To '.$end_request_month;
        
        $members = \App\dining\New_member::where('month_of_join',$m)->where('year_of_join',$y)->get();
        
        $in_this_month = \App\dining\Cost::where('cost_type',1)->whereBetween('date', [$start_request_month, $end_request_month])->sum('amount');
        $up_to_last_month_cost = \App\dining\Cost::where('cost_type',2)->whereBetween('date', [$start_request_month, $end_request_month])->sum('amount');
        $transper_to_next_month = \App\dining\Cost::where('cost_type',3)->whereBetween('date', [$start_request_month, $end_request_month])->sum('amount');
        $net_cost = $up_to_last_month_cost+$in_this_month-$transper_to_next_month;
        $meal=\App\dining\Meal::whereBetween('date', [$start_request_month, $end_request_month])->sum('no_of_meal');
                if($meal >0){
                       $result = round($net_cost/$meal,2); 
                    }else{
                       $result=0; 
                    }
       
        
        return view('dining.report.member_wise_report')
                ->withConfirm($confirm)
                ->withReport($report)
                ->withMembers($members)
                ->withM($m)->withY($y)
                ->withResult($result)
                ->withIn_this_month($in_this_month)
                ->withUp_to_last_month_cost($up_to_last_month_cost)
                ->withTransper_to_next_month($transper_to_next_month)
                ->withMeal($meal);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
