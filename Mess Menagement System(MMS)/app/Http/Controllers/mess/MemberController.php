<?php

namespace App\Http\Controllers\mess;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Member;
use Session;

class MemberController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
//    public function __construct() {
//        $this->middleware('auth');
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->middleware('auth');
       if($request->present_condition==true){
          $members = Member::where('present_condition',$request->present_condition)->paginate(4); 
       }  else {
           $members = Member::where('present_condition','running')->paginate(4);  
       }
       
        return view('members.index')->withMembers($members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->middleware('auth');
        return view('members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->middleware('auth');
        $this->validate($request,[
            'code_no'=>'required|unique:members|max:12|min:4',
            'name'=>'required|max:25|min:4',
            'contact_no'=>'required',
            'join_date'=>'required',
            'present_condition'=>'required'
        ]);
        
        $member= new Member();
        $member->code_no = $request->code_no;
        $member->name = $request->name;
        $member->contact_no = $request->contact_no;
        $member->email = $request->email;
        $member->address = $request->address;
        $member->join_date = $request->join_date;
        $member->present_condition = $request->present_condition;
        $member->save();
        Session::flash('success','Data save  successfully !');
        return redirect()->route('members.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        return view('members.edit')->withMember($member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::find($id);
        if($request->input('code_no')==$member->code_no){
          $this->validate($request,[
            'name'=>'required|max:25|min:4',
            'contact_no'=>'required',
            'join_date'=>'required',
            'present_condition'=>'required'
        ]);  
        }  else {
           $this->validate($request,[
            'code_no'=>'required|unique:members|max:12|min:4',
            'name'=>'required|max:25|min:4',
            'contact_no'=>'required',
            'join_date'=>'required',
            'present_condition'=>'required'
        ]); 
        }
        
        $member= Member::find($id);
        $member->code_no = $request->code_no;
        $member->name = $request->name;
        $member->contact_no = $request->contact_no;
        $member->email = $request->email;
        $member->address = $request->address;
        $member->join_date = $request->join_date;
        $member->present_condition = $request->present_condition;
        $member->releases_date = $request->releases_date;
        $member->save();
        Session::flash('success','Data Update  successfully !'.$request->present_condition);
        return redirect()->route('members.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
