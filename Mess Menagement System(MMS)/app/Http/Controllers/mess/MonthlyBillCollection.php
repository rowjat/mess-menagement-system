<?php

namespace App\Http\Controllers\mess;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Monthly_bill_collection;
use Session;
use App\Bill_category;
use App\Member;

class MonthlyBillCollection extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = Bill_category::all();
        return view('bill_collections.index')->withBills($bills);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bills = Bill_category::all();
        $members = Member::all();
        return view('bill_collections.create')->withBills($bills)->withMembers($members);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'bill_categories_id'=>'required',
            'member_id'=>'required',
            'collection_date'=>'required',
            'year'=>'required',
            'month'=>'required',
            'payable_bill'=>'required',
             'bill_pay'=>'required'
        ]);
        if($request->bill_categories_id > 0 && $request->member_id > 0){
//            $month = $request->month;
//            $year =  $request->year;
                    
            $count = Monthly_bill_collection::where('month',$request->month)->where('year',$request->year)->where('member_id',$request->member_id)->count();
            if($count > 0){
                 Session::flash('fails', 'This Month Allready Bill Pay');
                return redirect()->route('bill_collections.create');
            }else{
                if($request->payable_bill == $request->bill_pay){
                    
//                }
            $member= new Monthly_bill_collection();
            if($request->payment_options == 1)  {
                $member->bill_categories_id = $request->bill_categories_id;
                $member->member_id = $request->member_id;
                $member->collection_date = $request->collection_date;
                $member->year = $request->year;
                $member->month = $request->month;
                $member->payable_bill = $request->payable_bill;
                $member->payment_options = $request->payment_options;
                $member->bill_pay = $request->bill_pay;
                $member->save();
                Session::flash('success', 'Data save  successfully !');
                return redirect()->route('bill_collections.create');
            } else {
                $adjusts = \App\Monthly_advanced_collection::where('bill_categories_id',$request->bill_categories_id)->where('member_id',$request->member_id)->get();
                if($adjusts->count() >0){
                    foreach ($adjusts as $adjust){
                        $id = $adjust->id;
                    }
                   
                    $adjust = \App\Monthly_advanced_collection::find($id);
                    if($request->bill_pay <= $adjust->amount ){
                        $adjust->decrement('amount', $request->bill_pay);
                        //collection
                        $member->bill_categories_id = $request->bill_categories_id;
                $member->member_id = $request->member_id;
                $member->collection_date = $request->collection_date;
                $member->year = $request->year;
                $member->month = $request->month;
                $member->payable_bill = $request->payable_bill;
                $member->payment_options = $request->payment_options;
                $member->bill_pay = $request->bill_pay;
                $member->save();
                Session::flash('success', 'Data save  successfully !');
                return redirect()->route('bill_collections.create');
                        
                    }  else {
                        Session::flash('fails', 'You Have No enagh Advenced');
                return redirect()->route('bill_collections.create');
                    }
                    
                    Session::flash('success', $id);
                return redirect()->route('bill_collections.create');
                }else{
                   Session::flash('fails', 'You Have No Advenced');
                return redirect()->route('bill_collections.create');
                }
            }
            }  else {
                Session::flash('fails', 'Must Be Bill Pay equel Payable Bill');
                return redirect()->route('bill_collections.create');
            }
        }
        
        
        }  else {
            Session::flash('fails','Pls Select field !');
        return redirect()->route('bill_collections.create');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
