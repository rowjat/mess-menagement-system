<?php

namespace App\Http\Controllers\mess;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Monthly_bill_collection;
use App\Room;
use App\Member;
use App\Monthly_advanced_collection;
class AjexController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    public function bill_collection_index(Request $request)  {
//      $request = $request->all();
        $month = $request['month'];
        $year = $request['year'];
        $counts = Monthly_bill_collection::where('bill_categories_id', $request = $request['bill'])->where('month', $month)->where('year', $year)->get();
        if ($counts->count() > 0) {
            $data = "";
            $data .="<table class='table table-striped table-hover'>";
            $data .="<tr style='font-weight: bold;text-align: center;background-color: #737373;color: #EEE'><td>Room No</td><td>Name Of Member</td><td>Payable</td><td>Pay</td><td>Advence</td><td>Payment Option</td></tr>";

            foreach ($counts as $count) {
                $room = Room::find($count->members->id);
                
                $data .="<tr style='font-weight: bold;text-align: center;'>";
                $data .="<td>" . $room->room_no . "</td>";
                $data .="<td>" . $count->members->name . "</td>";
                $data .="<td>" . $count->payable_bill . "</td>";
                $data .="<td>" . $count->bill_pay . "</td>";
                $members = Monthly_advanced_collection::find($count->members->id);
                if($members==true){
                   $data .="<td>" . $members->amount . "</td>"; 
                }  else {
                    $data .="<td>" . "" . "</td>";
                }
                
                $payment_options = $count->payment_options == 1?'Current':'Adjust';
                $data .="<td>" . $payment_options. "</td>";
//                $data .="<td><a href='bill_collections/$count->id/edit'><button class='btn btn-sm btn-default '>EDIT</button></a></td>";
                $data .="</tr>";
            }

            $data .="</table>";
            return $data;
        } else {
            return 'No Any Data.......';
        }
    }
    
    public function advanced_index(Request $request)  {
        $bill = $request['id'];
        $counts = Monthly_advanced_collection::where('bill_categories_id', $bill)->get();
        if ($counts->count() > 0) {
            $data = "";
            $data .= "<table class='table table-striped table-hover'>";
            $data .= "<tr style='font-weight: bold;text-align: center;background-color: #737373;color: #EEE'><td>Code</td><td>Name</td><td>Advanced</td><td></td></tr>";
            foreach ($counts as $count){
            $data .= "<tr style='font-weight: bold;text-align: center'>";
            $data .= "<td>".$count->members->code_no."</td>";
            $data .= "<td>".$count->members->name."</td>";
            $data .= "<td>".$count->amount."</td>";
//            <a href='advanced_collections?page=2' 'id='$count->id'><button class='btn btn-sm btn-default '>ADD Advanced</button></a>
            $data .= "<td><a href='advanced_collections/$count->id/edit'><button class='btn btn-sm btn-default '>ADD Advanced</button></a></td>";
            $data .= "</tr>";
            }
            $data .= "</table> ";
            
            return $data; 
        }  else {
            return 'No Any Data.......';
        } 
    }
    public function member_wise_bill(){
        $members = Member::all();
        $bills = \App\Bill_category::all();
        return view('bill_collections.member_wise_bill')->withMembers($members)->withBills($bills);
    }
    public function member_wise_result(Request $request)  {
//      $request = $request->all();
        $member = $request['member'];
        $bill = $request['bill'];
        $counts = Monthly_bill_collection::where('member_id', $member)->where('bill_categories_id', $bill)->get();
        if ($counts->count() > 0) {
            $data = "";
            $data .="<table class='table table-striped table-hover'>";
            $data .="<tr style='font-weight: bold;text-align: center;background-color: #737373;color: #EEE'><td>Month</td><td>Payable Bill</td><td>Pay</td><td>payment_options</td></tr>";

            foreach ($counts as $count) {
                $data .="<tr style='font-weight: bold;text-align: center'>";
                $data .="<td>" .date('M', mktime(0, 0, 0, $count->month)) ."-"."$count->year" . "</td>";
                $data .="<td>" . $count->payable_bill . "</td>";
                $data .="<td>" . $count->bill_pay . "</td>";
                $payment_options = $count->payment_options == 1?'Current':'Adjust';
                $data .="<td>" . $payment_options. "</td>";
//                $data .="<td><a href='#'><button class='btn btn-sm btn-default '>EDIT</button></a></td>";
                $data .="</tr>";
            }

            $data .="</table>";
            return $data;
        } else {
            return 'No Any Data.......';
        }
    }
    
}

        
   
                    
                        
                   
                    
                        
                        
                        
                        
                    
                   
                                
                    
                        
                        
                        
                        
                        
                        
                    
                  
                