<?php

namespace App\Http\Controllers\mess;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Allocate_room;
use App\Member;
use App\Room;
use Session;
class AllocateRoomController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allocates = Allocate_room::orderBy('room_id', 'asc')->get();
        return view('allocate_rooms.index')->withAllocates($allocates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
       $members = Member::all();
       $rooms = Room::all();
       return view('allocate_rooms.create')->withMembers($members)
               ->withRooms($rooms); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'room_id'=>'required',
            'member_id'=>'required|unique:allocate_rooms'
        ]);
         if($request->room_id > 0 && $request->member_id > 0){
             $count = Allocate_room::where('room_id',$request->room_id)->where('member_id',$request->member_id)->count();
        if($count >0){
            Session::flash('fails','This Member Already taken in This Room !');
        return redirect()->route('allocate_rooms.create');
        }else{
            $member= new Allocate_room();
        $member->room_id = $request->room_id;
        $member->member_id = $request->member_id;
        $member->save();
        Session::flash('success','Data save  successfully !');
        return redirect()->route('allocate_rooms.create');
         }
        
        }  else {
            Session::flash('fails','Pls Select field !');
        return redirect()->route('allocate_rooms.create');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rooms = Room::all();
        $members = Member::all();
        $room = Allocate_room::find($id);
        return view('allocate_rooms.edit')->withRoom($room)->withRooms($rooms)->withMembers($members);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
