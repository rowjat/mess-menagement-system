<?php

namespace App\Http\Controllers\mess;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Monthly_advanced_collection;
use App\Bill_category;
use App\Member;
use Session;
class AdvancedCollectionController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = Bill_category::all();
        return view('advanced_collections.index')->withBills($bills);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bills = Bill_category::all();
        $members = Member::all();
        return view('advanced_collections.create')->withBills($bills)->withMembers($members);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'bill_categories_id'=>'required',
            'member_id'=>'required',
            'collection_date'=>'required'
        ]);
        $count = Monthly_advanced_collection::where('bill_categories_id',$request->bill_categories_id)->where('member_id',$request->member_id)->count();
       if($count >0){
           Session::flash('fails','In This Bill allready be taken this Member');
        return redirect()->route('advanced_collections.create');
       }else{
           if($request->bill_categories_id > 0 && $request->member_id > 0){
            $member= new Monthly_advanced_collection();
        $member->bill_categories_id = $request->bill_categories_id;
        $member->member_id = $request->member_id;
        $member->collection_date = $request->collection_date;
        $member->amount = $request->amount;
        $member->save();
        Session::flash('success','Data save  successfully !');
        return redirect()->route('advanced_collections.create');
        }  else {
            Session::flash('fails','Pls Select field !');
        return redirect()->route('advanced_collections.create');
        }
       }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advanced = Monthly_advanced_collection::find($id);
        return view('advanced_collections.edit')->withAdvanced($advanced);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $amount = $request->amount;
        $table = Monthly_advanced_collection::find($id);
//        DB::table('amount')->increment('votes', 5);
        $table->increment('amount', $amount);
        Session::flash('success',$amount.'/- Addvenced Add Successfull!');
        return redirect()->route('advanced_collections.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
