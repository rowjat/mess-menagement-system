<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return view('home.home');
});

Route::GET('bill_collection_index', 'mess\AjexController@bill_collection_index');
Route::GET('advanced_index', 'mess\AjexController@advanced_index');
Route::GET('member_wise_bills', 'mess\AjexController@member_wise_bill');
Route::GET('member_wise_result', 'mess\AjexController@member_wise_result');



Route::resource('members','mess\MemberController');
Route::resource('rooms','mess\RoomController');
Route::resource('allocate_rooms','mess\AllocateRoomController');
Route::resource('bill_categorys','mess\BillCategoryController');
Route::resource('bill_collections','mess\MonthlyBillCollection');
Route::resource('advanced_collections','mess\AdvancedCollectionController');

Route::get('profile', ['middleware' => 'auth', function() {


}]);
/*
 * dining 
 */
Route::resource('dining_members','dining\MemberController');
Route::resource('dining_diposit_withdraw','dining\DipositWithdrawController');
Route::resource('dining_cost','dining\CostController');
Route::resource('dining_meals','dining\MealController');
Route::resource('dining_closing_date','dining\ClosingDateController');
/*
 * dining report
 */
Route::GET('dining_cost_rechived_payment', 'dining\ReportController@cost_rechived_payment');
Route::GET('dining_member_wise_report', 'dining\ReportController@member_wise_report');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');