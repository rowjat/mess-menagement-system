<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allocate_room extends Model
{
    protected $table= 'allocate_rooms';
    public function rooms(){
        return $this->belongsTo('App\Room','room_id');
    }
    public function members(){
        return $this->belongsTo('App\Member','member_id');
    }
}
