<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monthly_bill_collection extends Model
{
     protected $table= 'monthly_bill_collections';
    public function bill_categories(){
        return $this->belongsTo('App\Bill_category','bill_categories_id');
    }
    public function members(){
        return $this->belongsTo('App\Member','member_id');
    }
}
