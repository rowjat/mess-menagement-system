<?php

namespace App\dining;

use Illuminate\Database\Eloquent\Model;

class New_member extends Model
{
    protected $table= 'new_members';
    public function members(){
        return $this->belongsTo('App\Member','member_id');
    }
}
