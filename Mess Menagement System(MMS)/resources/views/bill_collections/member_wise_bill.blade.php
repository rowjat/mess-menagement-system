@extends('master')
@section('title','Member Wise Bill | Index')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Member Wise Bill</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">

                <div class="col-lg-6">
  </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                {!! Form::open(['class'=>'form-horizontal','id'=>'form']) !!}
                <div class="form-group">
                    {!! Form::label('member','Name Of Member :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        <?php
                    $member_arr = [];
                    foreach ($members as $member) {
                        $member_arr[$member->id] = $member->name;
                    }
                    
                    ?>
                    {!! Form::select('member',$member_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('bill','Name Of Bill :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        <?php
                    $bill_arr = [];
                    foreach ($bills as $bill) {
                        $bill_arr[$bill->id] = $bill->name;
                    }
                    
                    ?>
                    {!! Form::select('bill',$bill_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>

                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
        <input type='button' value='GO!' class="btn btn-default" id="member_wise_result" />
                    
    </div>
                     </div>
                    {!! Form::close() !!}
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id='result'>
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                <!------------------->
            </div>
        </div> 
    </div>
</div>
@endsection


