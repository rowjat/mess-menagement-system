@extends('master')
@section('title','Bill collection | Edit')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Bill collection.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'bill_collections.store','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('bill_categories_id','Bill Category :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     <?php
                    $bill_arr = [];
                    $bill_arr[0] = 'select field.';
                    foreach ($bills as $bill) {
                        $bill_arr[$bill->id] = $bill->name;
                    }
                    ?>
                    {!! Form::select('bill_categories_id',$bill_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('member_id','Member Code :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    <?php
                    $member_arr = [];
                    $member_arr[0] = 'select field.';
                    foreach ($members as $member) {
                        $member_arr[$member->id] = $member->code_no.'('.$member->name.')';
                    }
                    
                    ?>
                    {!! Form::select('member_id',$member_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    
                <div class="form-group">
                     {!! Form::label('collection_date','Collection Date :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     {!! Form::date('collection_date',null,['class'=>'form-control']) !!}
                    </div>
                    </div>
            <div class="form-group">
                    {!! Form::label('year','Year :',['class'=>'col-sm-4 control-label']) !!}
                   <div class="col-sm-8">
                    {!! Form::selectYear('year', 2013,2030,2016,['class'=>'form-control']) !!}
                    </div>
                    </div>
        <div class="form-group">
                    {!! Form::label('month','Month :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::selectMonth('month',1,['class'=>'form-control']) !!}
                    </div>
                    </div>
    <div class="form-group">
                    {!! Form::label('payable_bill','Payable Bill :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::number('payable_bill',null,['class'=>'form-control','placeholder'=>'Type payable_bill..']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('payment_options','Payment Option :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::select('payment_options',[1=>'Current',2=>'Adjust'],null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('bill_pay','Pay Bill :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::number('bill_pay',null,['class'=>'form-control','placeholder'=>'Type bill_pay..']) !!}
                    </div>
                    </div>
                    
                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
                    {!! Form::submit('Add Memvers',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




