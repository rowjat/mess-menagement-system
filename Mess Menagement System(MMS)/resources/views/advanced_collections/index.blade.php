@extends('master')
@section('title','Advanced | Index')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>List of Advanced</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                {!! Form::open(['method'=>'GET','route'=>'advanced_collections.index','class'=>'form-inline','id'=>'form'])!!}
                <div class="form-group">
                    <?php
                    $bill_arr = ['Select....!'];
                    foreach($bills as $bill){
                        $bill_arr[$bill->id]=$bill->name;
                    }
                    ?>
                    {!! Form::select('bill',$bill_arr,null,['class'=>'form-control','id'=>'advanced_index'])!!}
                </div>
        <!--<input type='button' value='GO!' class="btn btn-default" id="advanced_index" />-->
                {!! Form::close() !!}
                <div class="col-lg-6">
  </div>
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id='result'>
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                 <!--$members->render()--> 
            </div>
        </div> 
    </div>
</div>
@endsection




