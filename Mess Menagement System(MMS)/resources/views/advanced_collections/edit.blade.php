@extends('master')
@section('title','Advanced collection | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Advanced collection.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'PUT','route'=>['advanced_collections.update','id'=>$advanced],'class'=>'form-horizontal']) !!}
                    
                    
                
            <div class="form-group">
                    {!! Form::label('amount','Amount :',['class'=>'col-sm-4 control-label']) !!}
                   <div class="col-sm-8">
                    {!! Form::number('amount',null,['class'=>'form-control','placeholder'=>'Type Amount..']) !!}
                    </div>
                    </div>
                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
                    {!! Form::submit('Add Advenced',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



