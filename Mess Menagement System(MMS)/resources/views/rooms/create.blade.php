@extends('master')
@section('title','Room | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Add new Room.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'rooms.store','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                   {!! Form::label('room_no','Room No :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::text('room_no',null,['class'=>'form-control','placeholder'=>'Type room_no..']) !!}
                    </div>
                    </div>
                     <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
                    {!! Form::submit('Add Rooms',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



