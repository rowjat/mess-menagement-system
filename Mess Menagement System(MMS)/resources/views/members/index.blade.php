@extends('master')
@section('title','Member | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Member List</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                {!! Form::open(['method'=>'GET','route'=>'members.index','class'=>'form-inline'])!!}
                <div class="form-group">
                    {!! Form::select('present_condition',['running'=>'Running','releases '=>'Releases'],null,['class'=>'form-control'])!!}
                </div>  
        {!! Form::submit("GO!",['class'=>'btn btn-primary','aria-hidden'=>'true']) !!}
                {!! Form::close() !!}
                <div class="col-lg-6">
  </div>
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <table class="table table-striped table-hover">
                    <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                        <td>Code</td>
                        <td>Name</td>
                        <td>Contact No</td>
                        <td>E-mail</td>
                        <td>Address</td>
                        <td>Join Date</td>
                        <td>Present Condition</td>
                        <td></td>
                    </tr>
                    @foreach($members as $member)
                    
                    <tr>
                        <td>{{$member->code_no}}</td>
                        <td>{{$member->name}}</td>
                        <td>{{$member->contact_no}}</td>
                        <td>{{$member->email}}</td>
                        <td>{{$member->address}}</td>
                        <td>{{$member->join_date}}</td>
                        <td>{{$member->present_condition}}<?php echo $member->present_condition=='running'?"":" (".$member->releases_date.")"; ?></td>
                        <td>{!! Html::linkRoute('members.edit','Edit',[$member->id],['class'=>'btn btn-sm btn-default']) !!}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                {!! $members->render() !!}
            </div>
        </div> 
    </div>
</div>
@endsection

