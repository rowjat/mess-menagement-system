@extends('master')
@section('title','Member | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Add new Members.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'members.store','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('code_no','Code :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::text('code_no',null,['class'=>'form-control','placeholder'=>'Type Code..']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('name','Name :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::text('name',null,['class'=>'form-control' ,'placeholder'=>'Type Name..']) !!}
                    </div>
                    </div>
                <div class="form-group">
                     {!! Form::label('contact_no','Contact NO :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                     {!! Form::text('contact_no',null,['class'=>'form-control' ,'placeholder'=>'Type Contact NO..']) !!}
                    </div>
                    </div>
            <div class="form-group">
                    {!! Form::label('email','E-mail :',['class'=>'col-sm-3 control-label']) !!}
                   <div class="col-sm-8">
                    {!! Form::email('email',null,['class'=>'form-control' ,'placeholder'=>'Type email..']) !!}
                    </div>
                    </div>
        <div class="form-group">
                    {!! Form::label('address','Address :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::textarea('address',null,['class'=>'form-control' ,'placeholder'=>'Type address..','style'=>'height: 60px']) !!}
                    </div>
                    </div>
    <div class="form-group">
                    {!! Form::label('join_date','Join Date :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::date('join_date',null,['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <input type="hidden" name="present_condition" value="running">
                     <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
                    {!! Form::submit('Add Memvers',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection