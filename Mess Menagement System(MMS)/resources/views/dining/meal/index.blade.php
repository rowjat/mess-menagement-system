@extends('master')
@section('title','Dining Meal | Index')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Individual Meals</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                
            </div>
        </div><hr/>
        
        <div class="row">
           
            <div class="col-md-6">
                {!! Form::open(['method'=>'GET','route'=>'dining_meals.index','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('member_id','Member Code :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     <?php
                    $member_arr= [];
                    foreach ($members as $member) {
                        $member_arr[$member->member_id] = $member->members->code_no." ( ".$member->members->name." )";
                    }
                    ?>
                    {!! Form::select('member_id',$member_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('month','Month :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::selectMonth('month',date('m'),['class'=>'form-control']) !!}
                    </div>
                    </div>
                <div class="form-group">
                     {!! Form::label('year','Year :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     {!! Form::selectYear('year', 2013,2030,date('Y'),['class'=>'form-control']) !!}
                    </div>
                    </div>

                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
        {!! Form::submit('GO!',['class'=>'btn btn-default']) !!}
                    
    </div>
                     </div>
                    {!! Form::close() !!}
            </div>
        </div><hr/>
        <div class="row ">
            <div class="col-md-8 col-md-offset-2">
                @if($confirm==1)
                <table class="table table-striped table-hover" style="border: 2px solid #737373">
                    <tr style="font-weight: bold;text-align: right;color: #737373;">
                        <td colspan="3">
                            Report from {{$report}}
                        </td>
                    </tr>
                    <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                        <td>Date</td>
                        <td>No Of Meals</td>
                        <td></td>
                    </tr>
                    <?php
                    $i=0;
                    $tx=0;
                    ?>
                @foreach($datas as $data)
                <?php
                $tx+=$data->no_of_meal;
                ?>
                    <tr style="font-weight: bold;text-align: center">
                        <td>{{$data->date}}</td>
                        <td>{{$data->no_of_meal}}</td>
                        <td>{!! Html::linkRoute('dining_cost.edit','Edit',[1],['class'=>'btn btn-sm btn-default']) !!}</td>
                    </tr>
                @endforeach
                   <tr style="font-weight: bold;text-align: center;background-color: #aaa;color: #EEE">
                       <td colspan="1">Total</td>
                       <td>{{$tx}}</td>
                       <td></td>
                   </tr>
                </table>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                
            </div>
        </div> 
    </div>
</div>
@endsection









