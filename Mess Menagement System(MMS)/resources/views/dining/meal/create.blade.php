@extends('master')
@section('title','Dining_Meal | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Meal Entry Form.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
               <!---------------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'dining_meals.store','class'=>'form-horizontal','id'=>'add_meals']) !!}
                    <div class="form-group">
                    {!! Form::label('date','Date :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-4">
                    {!! Form::date('date',null,['class'=>'form-control','required'=>""]) !!}
                    </div>
                    </div>
                    <!--------------------->
                    <div class="add_fild">
                       
                        <div class="form-group" id="row">
                            
                                {!! Form::label('member_id','Member Code :',['class'=>'col-sm-3 control-label']) !!}
                            
                            <div class="col-sm-4">
                                <select class="form-control" id="member_id" name="member_id[]">
                                    @foreach($meal_members as $member)
                                    <option value="{{$member->member_id}}">{{$member->members->code_no." ( ".$member->members->name.' )'}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="no_of_meal" min=1 max=10 required="" name="no_of_meal[]" placeholder="no of meal">
                            </div>
                            <div class="col-sm-1">
                                <span class="glyphicon glyphicon-plus btn btn-sm btn-success" aria-hidden="true" id="add"></span>
                            </div>
                        </div>
                        
                    </div>
                    <!-------------------->
                    
                     <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
                    {!! Form::submit('Save',['class'=>'btn btn-default','id'=>'save_meal']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection