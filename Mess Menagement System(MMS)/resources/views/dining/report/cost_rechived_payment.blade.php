@extends('master')
@section('title','Dining Meal | Index')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Report Of Cash Rechived & Payment</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                
            </div>
        </div><hr/>
        
        <div class="row">
           
            <div class="col-md-6">
                {!! Form::open(['method'=>'GET','url'=>'dining_cost_rechived_payment','class'=>'form-horizontal']) !!}
                    
                    <div class="form-group">
                    {!! Form::label('month','Month :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::selectMonth('month',date('m'),['class'=>'form-control']) !!}
                    </div>
                    </div>
                <div class="form-group">
                     {!! Form::label('year','Year :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     {!! Form::selectYear('year', 2013,2030,date('Y'),['class'=>'form-control']) !!}
                    </div>
                    </div>

                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
        {!! Form::submit('GO!',['class'=>'btn btn-default']) !!}
                    
    </div>
                     </div>
                    {!! Form::close() !!}
            </div>
        </div><hr/>
        <div class="row ">
            <div class="col-md-8 col-md-offset-2">
                @if($confirm==1)
                <table class="table table-striped table-hover" style="border: 2px solid #737373">
                    <tr style="font-weight: bold;text-align: right;color: #737373;">
                        <td colspan="5">
                            Report from {{$report}}
                        </td>
                    </tr>
                    <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                        <td>Member Code</td>
                        <td>Name Of Member</td>
                        <td>Diposit</td>
                        <td>Withdraw</td>
                        <td>Balance</td>
                    </tr>
                    <?php
                    $tx=0;
                    $ty=0;
                    ?>
                @foreach($members as $member)
                
                <?php
                
                $start_request_month=date("$y-$m-01");
                $end_request_month=date("$y-$m-t");
                $diposit = App\dining\Diposit_or_withdraw::where('member_id',$member->member_id)->where('option',1)->whereBetween('date', [$start_request_month, $end_request_month])->get();
                $withdraw = App\dining\Diposit_or_withdraw::where('member_id',$member->member_id)->where('option',2)->whereBetween('date', [$start_request_month, $end_request_month])->get();
                $tx+=$diposit->sum('amount');
                $ty+=$withdraw->sum('amount');
                ?>
                    <tr style="font-weight: bold;text-align: center">
                        <td>{{$member->members->code_no}}</td>
                        <td>{{$member->members->name}}</td>
                        <td>{{$diposit->sum('amount')}}</td>
                        <td>{{$withdraw->sum('amount')}}</td>
                        
                        <td>{{$diposit->sum('amount')-$withdraw->sum('amount')}}</td>
                    </tr>
                @endforeach
                   <tr style="font-weight: bold;text-align: center;background-color: #aaa;color: #EEE">
                       <td colspan="2">Total</td>
                       <td>{{$tx}}</td>
                       <td>{{$ty}}</td>
                       <td>{{$tx-$ty}}</td>
                   </tr>
                </table>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                
            </div>
        </div> 
    </div>
</div>
@endsection











