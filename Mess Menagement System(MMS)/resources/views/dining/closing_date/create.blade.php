@extends('master')
@section('title','Dining_Closing Date | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Set Closing Date.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                
            </div>
        </div><hr/>
        @if($count >0)
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <table>
                    @foreach($closings as $closing)
                    
                    <tr>
                        <td class="form-control col-md-4 text-center" style="font-weight: bold">{{$closing->date}}</td>
                    <td>-</td>
                    <td>{!! Html::linkRoute('dining_closing_date.edit','Set Closing Date',[$closing->id],['class'=>'btn btn-sm btn-success']) !!}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'dining_closing_date.store','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('date','Set Closing Date :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                    {!! Form::date('date',null,['class'=>'form-control']) !!}
                    </div>
                    </div>
                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
                    {!! Form::submit('Set Closing Date',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
        @endif
        
        
    </div>
</div>
@endsection