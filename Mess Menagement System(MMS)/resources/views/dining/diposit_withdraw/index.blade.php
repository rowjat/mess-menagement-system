@extends('master')
@section('title','Cash Rechived Payment | Index')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Cash Rechived Payment</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                
            </div>
        </div><hr/>
        
        <div class="row">
           
            <div class="col-md-6">
                {!! Form::open(['method'=>'GET','route'=>'dining_diposit_withdraw.index','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('member_id','Member Code :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     <?php
                    $member_arr= [];
                    foreach ($members as $member) {
                        $member_arr[$member->member_id] = $member->members->code_no." ( ".$member->members->name." )";
                    }
                    ?>
                    {!! Form::select('member_id',$member_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('start_date','Start Date :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::date('start_date',null,['class'=>'form-control','required'=>'']) !!}
                    </div>
                    </div>
                <div class="form-group">
                     {!! Form::label('end_date','End Date :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     {!! Form::date('end_date',null,['class'=>'form-control']) !!}
                    </div>
                    </div>

                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
        {!! Form::submit('GO!',['class'=>'btn btn-default']) !!}
                    
    </div>
                     </div>
                    {!! Form::close() !!}
            </div>
        </div><hr/>
        <div class="row ">
            <div class="col-md-8 col-md-offset-2">
                @if($confirm==1)
                <table class="table table-striped table-hover" style="border: 2px dotted #737373">
                    <tr style="font-weight: bold;text-align: right;color: #737373;">
                        <td colspan="5">
                            Report from {{$report}}
                        </td>
                    </tr>
                    <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                        <td>Date</td>
                        <td>Diposit</td>
                        <td>Withdraw</td>
                        <td>Balance</td>
                        <td>{{date('m-t-Y')}}</td>
                    </tr>
                    <?php
                    $i=0;
                    $tx=0;
                    $ty=0;
                    ?>
                   @foreach($cashs as $cash)
                  <?php
                    $x=$cash->option==1?$cash->amount:0;
                    $y=$cash->option==2?$cash->amount:0;
                    $i+=$x-$y;
                    $tx+=$x;
                    $ty+=$y;
                    ?>
                    <tr style="font-weight: bold;text-align: center">
                        <td>{{$cash->date}}</td>
                        <td>{{$cash->option==1?$cash->amount:0}}</td>
                       
                        <td>{{$cash->option==2?$cash->amount:0}}</td>
                       
                        <td>{{$i}}</td>
                        <td>{!! Html::linkRoute('dining_diposit_withdraw.edit','Edit',[1],['class'=>'btn btn-sm btn-default']) !!}</td>
                    </tr>
                   @endforeach 
                   <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                       <td>Total</td>
                       <td>{{$tx}}</td>
                       <td>{{$ty}}</td>
                       <td>{{$tx-$ty}}</td>
                       <td></td>
                   </tr>
                </table>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                
            </div>
        </div> 
    </div>
</div>
@endsection





