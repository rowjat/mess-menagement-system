@extends('master')
@section('title','Dining_diposit_withdraw | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Diposit Or Withdraw.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!----------------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'dining_diposit_withdraw.store','class'=>'form-horizontal']) !!}
                  
                    <div class="form-group">
                    {!! Form::label('member_id','Member Code :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    <?php
                    $member_arr= ['select field.'];
                    foreach ($members as $member) {
                        $member_arr[$member->member_id] = $member->members->code_no.'('.$member->members->name.')';
                    }
                    ?>
                    {!! Form::select('member_id',$member_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('option','Option :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::select('option',[1=>'Diposit',2=>'Withdraw'],null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('date','Date :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::date('date', 'null',['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('amount','Amount :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::number('amount', 'null',['class'=>'form-control']) !!}
                    </div>
                    </div>
                     <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
                    {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection