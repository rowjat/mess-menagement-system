@extends('master')
@section('title','Daining_Member | Index')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Set Closing Date.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                
            </div>
        </div><hr/>
        
        <div class="row">
           
            <div class="col-md-6">
                {!! Form::open(['method'=>'GET','route'=>'dining_members.index','class'=>'form-horizontal']) !!}
                    
                    <div class="form-group">
                    {!! Form::label('month','Name Of Month :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::selectMonth('month',date('m'),['class'=>'form-control']) !!}
                    </div>
                    </div>
                <div class="form-group">
                     {!! Form::label('year','Name Of Year :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     {!! Form::selectYear('year', 2013,2030,date('Y'),['class'=>'form-control']) !!}
                    </div>
                    </div>

                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
        {!! Form::submit('GO!',['class'=>'btn btn-default']) !!}
                    
    </div>
                     </div>
                    {!! Form::close() !!}
            </div>
        </div><hr/>
        <div class="row ">
            
            <div class="col-md-8 col-md-offset-2">
                <table class="table table-striped table-hover" style="border: 2px solid #737373">
                    <tr style="font-weight: bold;text-align: right;color: #737373;">
                        <td colspan="3">
                            Report of {{date('F', mktime(0, 0, 0, $m))}}-{{$y}}
                        </td>
                    </tr>
                    <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                        <td>Code</td>
                        <td>Name</td>
                        <td></td>
                    </tr>
                    @foreach($members as $member)
                    <tr style="font-weight: bold;text-align: center">
                        <td>{{$member->members->code_no}}</td>
                        <td>{{$member->members->name}}</td>
                        <td>{!! Html::linkRoute('dining_members.edit','Edit',[$member->id],['class'=>'btn btn-sm btn-default']) !!}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                
            </div>
        </div> 
    </div>
</div>
@endsection



