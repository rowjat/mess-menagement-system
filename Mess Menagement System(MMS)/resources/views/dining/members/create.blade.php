@extends('master')
@section('title','Dining_Member | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Join new Members.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
               
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'dining_members.store','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('month_of_join','Month Of Join :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::selectMonth('month_of_join',date('m'),['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('year_of_join','Year Of Join :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::selectYear('year_of_join', 2013,2030,date('Y'),['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('member_id','Member Code :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    <?php
                    $member_arr= ['select field.'];
                    foreach ($members as $member) {
                        $member_arr[$member->id] = $member->code_no.'('.$member->name.')';
                    }
                    ?>
                    {!! Form::select('member_id',$member_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                     <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
                    {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection