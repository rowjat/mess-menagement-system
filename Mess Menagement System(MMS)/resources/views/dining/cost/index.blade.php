@extends('master')
@section('title','Dining Cost | Index')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Costing List</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                
            </div>
        </div><hr/>
        
        <div class="row">
           
            <div class="col-md-6">
                {!! Form::open(['method'=>'GET','route'=>'dining_cost.index','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('cost_type','Type Of Cost :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::select('cost_type',[1=>'Running Cost',2=>'From Up To Last Month Cost',3=>'Transper To In This Month Cost'],null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('month','Month :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::selectMonth('month',date('m'),['class'=>'form-control']) !!}
                    </div>
                    </div>
                <div class="form-group">
                     {!! Form::label('year','Year :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                     {!! Form::selectYear('year', 2013,2030,date('Y'),['class'=>'form-control']) !!}
                    </div>
                    </div>

                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
        {!! Form::submit('GO!',['class'=>'btn btn-default']) !!}
                    
    </div>
                     </div>
                    {!! Form::close() !!}
            </div>
        </div><hr/>
        <div class="row ">
            <div class="col-md-8 col-md-offset-2">
                @if($confirm==1)
                <table class="table table-striped table-hover" style="border: 2px dotted #737373">
                    <tr style="font-weight: bold;text-align: right;color: #737373;">
                        <td colspan="6">
                            Report from {{$report}}
                        </td>
                    </tr>
                    <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                        <td>Date</td>
                        <td>Vawter No</td>
                        <td>Cost Description</td>
                        <td>Amount</td>
                        <td>Spenders</td>
                        <td></td>
                    </tr>
                    <?php
                    $i=0;
                    $tx=0;
                    ?>
                @foreach($datas as $data)
                <?php
                $tx+=$data->amount;
                $spenders = App\Member::find($data->spenders);
                ?>
                    <tr style="font-weight: bold;text-align: center">
                        <td>{{$data->date}}</td>
                        <td>{{$data->vawter_no}}</td>
                        <td>{{$data->drescription}}</td>
                       
                        <td>{{$data->amount}}</td>
                       
                        <td>{{$spenders->name}}</td>
                        <td>{!! Html::linkRoute('dining_cost.edit','Edit',[1],['class'=>'btn btn-sm btn-default']) !!}</td>
                    </tr>
                @endforeach
                   <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                       <td colspan="3">Total</td>
                       <td>{{$tx}}</td>
                       <td></td>
                       <td></td>
                   </tr>
                </table>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                
            </div>
        </div> 
    </div>
</div>
@endsection







