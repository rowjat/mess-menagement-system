@extends('master')
@section('title','Dining_Cost | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Cost Entry Form.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'dining_cost.store','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('date','Date :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::date('date',null,['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('vawter_no','Vawter No :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                   
                    {!! Form::text('vawter_no',null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('cost_type','Cost Type :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                   
                    {!! Form::select('cost_type',[1=>'Running Cost',2=>'From Up To Last Month Cost',3=>'Transper To In This Month Cost'],null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('drescription','Drescription :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::textarea('drescription', null,['class'=>'form-control','style'=>'height: 60px']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('amount','Amount :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::number('amount', 'null',['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('spenders','Spenders :',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                    <?php
                    $member_arr= ['select field.'];
                    foreach ($members as $member) {
                        $member_arr[$member->member_id] = $member->members->code_no.'('.$member->members->name.')';
                    }
                    ?>
                    {!! Form::select('spenders',$member_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                     <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
                    {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection