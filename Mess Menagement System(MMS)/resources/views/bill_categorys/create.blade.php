@extends('master')
@section('title','Bill_category | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>ADD BILL.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-10 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','route'=>'bill_categorys.store','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        {!! Form::label('name','Name Of Bill :',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Type Name Of Bill..']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('description','Description Of Bill :',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Type Description Of Bill..','style'=>'height: 60px']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



