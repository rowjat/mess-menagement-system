@extends('master')
@section('title','Registion | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Registion Form.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
               
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::open(['method'=>'POST','url'=>'/auth/register','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('name','Name :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('email','Email :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::email('email',null,['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('password','Password :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::password('password',['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('password_confirmation','Confirm Password :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                    {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
                    </div>
                    </div>
                     <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
                    {!! Form::submit('Register',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

