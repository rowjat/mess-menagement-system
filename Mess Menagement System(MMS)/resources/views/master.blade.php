<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layout._head')
    </head>
    <body>
        @include('layout._header')  
        <div class="row" >
            @include('layout._sideber')       
            <div class="col-md-9" >
                <div class="row" style="border-left: 1px dotted #eea236;padding-left: 10px">
                    @include('layout._nav') 
                    @include('layout._msg')
                    <div class="row">
                        <div class="col-md-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        @include('layout._footer')   
        @include('layout._js')    
    </body>
</html>