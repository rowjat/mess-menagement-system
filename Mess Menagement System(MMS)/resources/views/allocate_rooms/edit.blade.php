@extends('master')
@section('title','Allocate_Room | create')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Allocated Room.</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <!------------->
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 col-md-offset-1">
                    {!! Form::model($room,['method'=>'PUT','route'=>'allocate_rooms.update','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        {!! Form::label('room_id','Room No :',['class'=>'col-sm-3 control-label']) !!}
                    
                    <div class="col-sm-8">
                    <?php
                    $room_arr = [];
                    $room_arr[] = 'select field.';
                    foreach ($rooms as $room) {
                        $room_arr[$room->id] = $room->room_no;
                    }
                    ?>
                    {!! Form::select('room_id',$room_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('member_id','Member Code :',['class'=>'col-sm-3 control-label']) !!}
                    
                    <div class="col-sm-8">
                    <?php
                    $member_arr = [];
                    $member_arr[] = 'select field.';
                    foreach ($members as $member) {
                        $member_arr[$member->id] = $member->code_no.' ('.$member->name.')';
                    }
                    ?>
                    {!! Form::select('member_id',$member_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                
                     <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
                    {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
    </div>
                     </div>
                    {!! Form::close() !!} 
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




