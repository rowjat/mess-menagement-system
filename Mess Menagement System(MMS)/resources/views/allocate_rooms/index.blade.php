@extends('master')
@section('title','Allocate Room | Index')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9 col-md-offset-0 text-center">
                <h3>Allocate Room List</h3>
            </div>
            <div class="col-md-3 col-md-offset-0">
                <button class="btn btn-primary" style="font-weight: bold" id="present_cond">RUNNING</button>
            </div>
        </div><hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <table class="table table-striped table-hover">
                    <tr style="font-weight: bold;text-align: center;background-color: #737373;color: #EEE">
                        
                        <td>Room No</td>
                        <td>Member</td>
                        <td>Present Condition</td>
                    </tr>
                    @foreach($allocates as $alocate)
                    @if($alocate->members->present_condition == 'running')
                    <tr class="running" style="text-align: center">
                    @else
                    <tr class="releases" style="text-align: center">
                    @endif
                        <td>{{$alocate->rooms->room_no}}</td>
                        <td>{{$alocate->members->name}}</td>
                        <td>{{$alocate->members->present_condition}}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-offset-5">
                <!--------------------->
                 <!------------------->   
            </div>
        </div> 
    </div>
</div>
@endsection


