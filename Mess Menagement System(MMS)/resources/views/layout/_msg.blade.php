@if(Session::has('success'))
<div class="alert alert-success" role="alert">
    <strong>Success:</strong>{{Session::get('success')}}
</div>
@endif
@if(Session::has('fails'))
<div class="alert alert-danger" role="alert">
    <strong>Failds:</strong>{{Session::get('fails')}}
</div>
@endif
@if(count($errors)>0)
<div class="alert alert-danger" role="alert">
    <strong>Errors:
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
</div>
@endif