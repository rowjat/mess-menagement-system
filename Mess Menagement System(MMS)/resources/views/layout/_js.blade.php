<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
    //sideber
    $(document).ready(function(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //---------
        $('.has-sub').click(function(){
             $('.has-sub').removeClass('tap');
            $(this).addClass('tap');
        });
        //allocate room list
        $('#present_cond').click(function(){
            var button = $('#present_cond').html();
            
            console.log(button);
            if(button == 'RUNNING'){
                $('#present_cond').html('RELEASES');
                $('#present_cond').removeClass('btn btn-primary');
                $('#present_cond').addClass('btn btn-danger');
                $('.running').show();
                $('.releases').hide();
            }else{
                $('#present_cond').html('RUNNING');
                $('#present_cond').removeClass('btn btn-danger');
                $('#present_cond').addClass('btn btn-primary');
                $('.running').hide();
                $('.releases').show();
            }
            
        });


        //bill_collection_index
            $('#bill_collection_index').on('click', function () {
                $.ajax({
                    url: "{{action('mess\AjexController@bill_collection_index')}}",
                    type: "GET",
                    async: false,
                    data: $("#form").serialize(),
                    success: function (data) {
//                         console.log(data);
                         $("#showme").html(data);
                    }
                });
            });
         //advanced_index
            $('#advanced_index').on('change',function () {
                $.ajax({
                    url: "{{action('mess\AjexController@advanced_index')}}",
                    type: "GET",
                    async: false,
                    data: {
                        id:$('#advanced_index').val()
                    },
                    success: function (data) {
//                         console.log(data);
                         $("#result").html(data);
                    }
                });
            });
            //member_wise_result
            $('#member_wise_result').on('click', function () {
                $.ajax({
                    url: "{{action('mess\AjexController@member_wise_result')}}",
                    type: "GET",
                    async: false,
                    data: $("#form").serialize(),
                    success: function (data) {
//                         console.log(data);
                         $("#result").html(data);
                    }
                });
            });
            /*
             * daining
             */
            // daining_meal_add_input
            
            var i = 1;
            $('#add').click(function(){
                i++;
                 var form = '';
                      form +='<div class="form-group" id="row'+i+'">';
                      form +='<div class="col-sm-3 control-label" style="font-weight:bold">Member Code :</div>';
                      form +='<div class="col-sm-4">';
                      form +='<select class="form-control" id="member_id" name="member_id[]">';
                       @if(isset($meal_members)) 
                           @foreach($meal_members as $meal_member)
                           form +='<option value="'+{{$meal_member->member_id}}+'">'+'{{$meal_member->members->code_no}}'+' ( {{$meal_member->members->name}} )'+'</option>';
                           @endforeach
                       @endif
                      form +='</select>';
                      form +='</div>';
                      form +='<div class="col-sm-3">';
                      form +='<input type="number" class="form-control" min=1 max=10 required="" id="no_of_meal" name="no_of_meal[]" placeholder="no of meal">';
                      form +='</div>';
                      form +='<div class="col-sm-1">';
                      form +='<span class="glyphicon glyphicon-minus btn btn-sm btn-danger btn-remove" aria-hidden="true" id="'+i+'"></span>';
                      form +='</div>';
                      form +='</div>';        
                $('.add_fild').append(form);
            });
            $(document).on('click','.btn-remove',function(){
                var button_id = $(this).attr('id');
                $('#row'+button_id+'').remove();
            });
            
    })
</script>
    
