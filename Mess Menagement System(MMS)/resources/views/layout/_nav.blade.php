<nav class="navbar navbar-inverse" style="background-color:#EEE;" >

    <div class="container-fluid" >
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
         <!--Collect the nav links, forms, and other content for toggling--> 
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"  >
            <ul class="nav nav-tabs" >
                <li class="#"><a style="font-weight: bold;" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a></li>
                <li class="dropdown" >
                    <a style="font-weight: bold;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MESS <span class="caret"></span></a>
                    <ul class="dropdown-menu" >
                        <li><a href="{{route('members.index')}}">Member List</a></li>
                        <li><a href="{{route('allocate_rooms.index')}}">Allocate Room</a></li>
                        <li><a href="{{route('bill_collections.index')}}">Indivisul Bill</a></li>
                        <li><a href="{{route('advanced_collections.index')}}">Advanced</a></li>
                         <li><a href="{{url('member_wise_bills')}}">Member_wise_bills</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a style="font-weight: bold;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Daining<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('dining_members.index')}}">Member List</a></li>
                        <li><a href="{{route('dining_diposit_withdraw.index')}}">Cash Recived & Payment</a></li>
                        <li><a href="{{route('dining_cost.index')}}">Cost</a></li>
                        <li><a href="{{route('dining_meals.index')}}">Meals</a></li>
                        <li style="text-align: center;font-weight: bold;background-color: #269abc">report</li>
                      
                        <li><a href="{{url('dining_cost_rechived_payment')}}">Report Of Cash</a></li>
                        <li><a href="{{url('dining_member_wise_report')}}">Monthly Report</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

