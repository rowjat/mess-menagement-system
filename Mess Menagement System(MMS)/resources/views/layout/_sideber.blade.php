<div class="col-md-3" >
    <div class=""style="min-height: 600px;">
        <div class="text-center text-capitalize" style="color: #EEE;background-color: #737373;padding: 5px">
            <h3>Menu</h3>
        </div>
        <hr/>
        <div class="main-sid" style="">
            <ul class="main-sid-ul">
                <li class="has-sub"><a class="a" href="/">HOME</a>    
                </li>
                @if(Auth::check())
                <li class="has-sub"><a class="a" href="#"><span class="sub-arrow">MESS</span></a>
                    <ul>
                        <li><a href="{{Route('members.create')}}">JOIN MEMBER</a></li>
                        <li><a href="{{Route('rooms.create')}}">ADD NEW ROOM</a></li>
                        <li><a href="{{Route('allocate_rooms.create')}}">ALLOCATED ROOM</a></li>
                        <li><a href="{{Route('bill_categorys.create')}}">ADD BILL CATEGORY</a></li>
                        <li><a href="{{Route('bill_collections.create')}}">COLLECTION BILL</a></li>
                        <li><a href="{{Route('advanced_collections.create')}}">ADVANCED COLLECT</a></li>
                    </ul>    
                </li>
                <li class="has-sub"><a class="a" href="#"><span class="sub-arrow">DAINING</span></a>
                    <ul>
                        <li><a href="{{Route('dining_members.create')}}">JOIN MEMBER</a></li>
                        <li><a href="{{Route('dining_diposit_withdraw.create')}}">DIPOSIT OR WITHDRAW</a></li>
                        <li><a href="{{Route('dining_cost.create')}}">Cost Entry</a></li>
                        <li><a href="{{Route('dining_meals.create')}}">Meal Entry Form</a></li>
                        <li><a href="{{Route('dining_closing_date.create')}}">SET CLOSING DATE*</a></li>
                    </ul>    
                </li>
                @endif
                @if(!Auth::check())
                <li class="has-sub"><a class="a" href="{{url('auth/register')}}">REGISTION</a>   
                <li class="has-sub"><a class="a" href="{{url('auth/login')}}">LOGIN</a>
                @else
                <li class="has-sub"><a class="a" href="{{url('auth/logout')}}">LOGOUT</a>
                @endif      
            </ul>
        </div>
    </div>
</div>

